const pool = require('../utils/db');

const tbName = 'Product';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}";`;
    const result = await pool.query(sql);
    return result.rows;
  },
  allByCatId: async id => {
    const sql = `SELECT * FROM "${tbName}" WHERE "categoryId" = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows;
  },
  addProduct: async (
    name,
    image,
    sideImages,
    price,
    closedDate,
    description,
    increment,
    startingPrice,
    sellerId,
    categoryId
  ) => {
    const submittedDate = new Date();
    const sql = `INSERT INTO "${tbName}" (name, image, "sideImages", price, "submittedDate", "closedDate", description, increment, "startingPrice", "sellerId", "categoryId", document_vectors) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, (to_tsvector($12) || to_tsvector($13))) RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [
      name,
      image,
      sideImages,
      parseFloat(price),
      submittedDate,
      closedDate,
      description,
      parseFloat(increment),
      parseFloat(startingPrice),
      parseInt(sellerId),
      parseInt(categoryId),
      name,
      description
    ]);
    return result.rows[0];
  },
  updateProduct: async (
    id,
    name,
    image,
    sideImages,
    price,
    closedDate,
    description,
    increment,
    startingPrice,
    sellerId,
    categoryId
  ) => {
    const sql = `UPDATE "${tbName}" SET (name, image, "sideImages", price, "closedDate", description, increment, "startingPrice", "sellerId", "categoryId", document_vectors) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, (to_tsvector($11) || to_tsvector($12))) WHERE id = $13 RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [
      name,
      image,
      sideImages,
      parseFloat(price),
      closedDate,
      description,
      parseFloat(increment),
      parseFloat(startingPrice),
      parseInt(sellerId),
      parseInt(categoryId),
      name,
      description,
      id
    ]);
    return result.rows[0];
  },
  deleteProduct: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING id::int, name, price::float, "submittedDate", "closedDate", description, increment::float, "startingPrice"::float, "sellerId"::int, "categoryId"::int;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
