const pool = require('../../utils/db');

const tbName = 'Category';

module.exports = {
  rootList: async () => {
    const sql = `SELECT distinct cat1.id, cat1.name FROM "${tbName}" as cat1, "${tbName}" as cat2 WHERE cat1.id = cat2.root ORDER BY cat1.id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  catsOfRoot: async (id) => {
    const sql = `SELECT * FROM "${tbName}" WHERE root = $1;`;
    const result = await pool.query(sql,[id]);
    return result.rows;
  }
};
