const pool = require('../../utils/db');

const tbName = 'Product';

const tbBid = 'Bid';

const productbid = `SELECT "${tbName}".*, "bidProduct"."bId" as "bidderId", "bidProduct"."bCn" as "count", "bidProduct"."bMVl" as value FROM "${tbName}" left join (select "a"."bidderId" as "bId", "c"."maxValue" as "bMVl", "b"."productId" as "pId", "b"."count" as "bCn" FROM "${tbBid}" as "a", (SELECT "productId", count("productId") as "count" FROM "${tbBid}" GROUP BY "productId") as "b", (select "productId", max(value) as "maxValue" from "${tbBid}" GROUP BY "productId") as "c" WHERE "a"."productId" = "b"."productId" and "b"."productId" = "c"."productId" and "a".value = "c"."maxValue" ORDER BY "b"."productId" ASC) as "bidProduct" on "${tbName}".id = "bidProduct"."pId"`;

module.exports = {
  productBid: async () => {
    const sql = `${productbid} ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  searchByName: async (search) => {
    const sql = `SELECT * FROM (${productbid}) as "productbid" WHERE id = ${search} OR name LIKE '%${search}%' ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  searchFilterClosedDateDESC: async (search) => {
    const sql = `SELECT * FROM (${productbid}) as "productbid" WHERE id = ${search} OR name LIKE '%${search}%' WHERE "closedDate" >= current_timestamp ORDER BY ("closedDate"-current_timestamp) DESC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  searchFilterPriceASC: async (search) => {
    const sql = `SELECT * FROM (${productbid}) as "productbid" WHERE id = ${search} OR name LIKE '%${search}%' ORDER BY price ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  topGoToEnd: async () =>{
    const sql = `SELECT * FROM (${productbid}) as "productbid" WHERE "closedDate" >= current_timestamp ORDER BY ("closedDate"-current_timestamp) ASC LIMIT 5;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  topDeal: async () => {
    const sql = `select * FROM (${productbid}) as "productbid" ORDER BY "count" DESC LIMIT 5;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  topBestPrice: async () => {
    const sql = `SELECT * FROM (${productbid}) as "productbid" ORDER BY price ASC LIMIT 5;`;
    const result = await pool.query(sql);
    return result.rows;
  }
};