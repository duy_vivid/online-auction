const pool = require('../utils/db');

const tbName = 'Bid';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY approved;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  //TODO: get all bids by products
  addBid: async (bidderId, productId, value) => {
    const now = new Date();
    const sql = `INSERT INTO "${tbName}" ("bidderId", "productId", value, date) VALUES ($1, $2, $3, $4) RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [bidderId, productId, value, now]);
    return result.rows[0];
  },
  updateBid: async (id, bidderId, productId, value, approved) => {
    const sql = `UPDATE "${tbName}" SET ("bidderId", "productId", value, approved) VALUES ($1, $2, $3, $4) WHERE id = $5 RETURNING id::int, "bidderId"::int, "productId"::int, value::float, date, approved;`;
    const result = await pool.query(sql, [bidderId, productId, value, approved, id]);
    return result.rows[0];
  },
  deleteBid: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE "id" = $1 RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
