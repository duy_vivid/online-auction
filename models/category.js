const pool = require('../utils/db');

const tbName = 'Category';

module.exports = {
  all: async () => {
    const sql = `SELECT "${tbName}".id, "${tbName}".name, "rootCat".id AS "rootCatId", "rootCat".name AS "rootCatName" FROM "${tbName}" LEFT JOIN "${tbName}" AS "rootCat" ON "${tbName}".root = "rootCat".id;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  addCategory: async (name, root) => {
    if (root) {
      const sql = `INSERT INTO "${tbName}" (name, root) VALUES ($1,$2) RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, root]);
      return result.rows[0];
    } else {
      const sql = `INSERT INTO "${tbName}" (name) VALUES ($1) RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name]);
      return result.rows[0];
    }
  },
  updateCategory: async (id, name, root) => {
    if (root) {
      const sql = `UPDATE "${tbName}" SET (name, root) = ($1,$2) WHERE id = $3 RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, root, id]);
      return result.rows[0];
    } else {
      const sql = `UPDATE "${tbName}" SET (name) = ($1) WHERE id = $2 RETURNING id::int, name, root;`;
      const result = await pool.query(sql, [name, id]);
      return result.rows[0];
    }
  },
  deleteCategory: async id => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING id::int, name;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
