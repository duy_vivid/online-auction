const pool = require('../../utils/db');

const tbName = 'Request';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY "userId" ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  search: async (search) => {
    const sql = `SELECT * FROM "${tbName}" WHERE "userId" LIKE '%${search}%' ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  }
};
