const pool = require('../../utils/db');

const tbName = 'Category';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  search: async (search) => {
    const sql = `SELECT * FROM "${tbName}" WHERE lower(name) LIKE lower('%${search}%') ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  addCategory: async (name, root) => {
    const sql = `INSERT INTO "${tbName}" (name, root) VALUES ($1, $2) RETURNING *;`;
    const result = await pool.query(sql, [name, root]);
    return result.rows[0];
  },
  getEditCategory: async (id) => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  postEditCategory: async (id, name, root) => {
    const sql = `UPDATE "${tbName}" SET name = $2, root = $3 WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id, name, root]);
    return result.rows[0];
  },
  deleteCategory: async (id) => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
