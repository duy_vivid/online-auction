const pool = require('../../utils/db');

const tbName = 'Product';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  search: async (search) => {
    const sql = `SELECT * FROM "${tbName}" WHERE name LIKE '%${search}%' ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  addProduct: async (name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors) => {
    const sql = `INSERT INTO "${tbName}" (name, price, "submittedDate", "closedDate", description, increment, "startingPrice", "sellerId", "categoryId", document_vectors) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *;`;
    const result = await pool.query(sql, [name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors]);
    return result.rows[0];
  },
  getEditProduct: async (id) => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  postEditProduct: async (id, name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors) => {
    const sql = `UPDATE "${tbName}" SET name = $2, price = $3, "submittedDate" = $4, "closedDate" = $5, description = $6, increment = $7, "startingPrice" = $8, "sellerId" = $9, "categoryId" = $10, document_vectors = $11 WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id, name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors]);
    return result.rows[0];
  },
  deleteProduct: async (id) => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  }
};
