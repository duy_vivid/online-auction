const pool = require('../../utils/db');

const tbName = 'User';

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM "${tbName}" ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  search: async (search) => {
    const sql = `SELECT * FROM "${tbName}" WHERE name LIKE '%${search}%' ORDER BY id ASC;`;
    const result = await pool.query(sql);
    return result.rows;
  },
  searchEmail: async (search) => {
    const sql = `SELECT * FROM "${tbName}" WHERE email = '${search}';`;
    const result = await pool.query(sql);
    return result.rows[0];
  },
  addUser: async (name, email, password, role) => {
    const sql = `INSERT INTO "${tbName}" (name, email, password, role) VALUES ($1, $2, $3, $4) RETURNING *;`;
    const result = await pool.query(sql, [name, email, password, role]);
    return result.rows[0];
  },
  getEditUser: async (id) => {
    const sql = `SELECT * FROM "${tbName}" WHERE id = $1;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  postEditUser: async (id, name, email, password, role) => {
    const sql = `UPDATE "${tbName}" SET name = $2, email = $3, password  = $4, role = $5 WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id, name, email, password, role]);
    return result.rows[0];
  },
  deleteUser: async (id) => {
    const sql = `DELETE FROM "${tbName}" WHERE id = $1 RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  upgradeUser: async (id) => {
    const sql = `UPDATE "${tbName}" SET role = 'Seller' WHERE id = $1 and lower(role) = 'bidder' RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
  downgradeUser: async (id) => {
    const sql = `UPDATE "${tbName}" SET role = 'Bidder' WHERE id = $1 and lower(role) = 'seller' RETURNING *;`;
    const result = await pool.query(sql, [id]);
    return result.rows[0];
  },
};
