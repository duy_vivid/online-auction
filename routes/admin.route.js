const express = require('express');
const router = express.Router();

const adminUserRoute = require('./admin/user.route');
const adminProductRoute = require('./admin/product.route');
const adminRequestRoute = require('./admin/request.route');
const adminCategoryRoute = require('./admin/category.route');

router.use('/user', adminUserRoute);
router.use('/product', adminProductRoute);
router.use('/request', adminRequestRoute);
router.use('/category', adminCategoryRoute);

module.exports = router;