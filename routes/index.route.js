const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const controller = require('../controllers/index.controller');

router.get('/', controller.home);

router.get('/search', controller.search);

router.get('/list', controller.list);

module.exports = router;