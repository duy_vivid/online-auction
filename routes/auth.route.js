const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const controller = require('../controllers/auth.controller');

router.get('', controller.login);

router.post('', urlencodedParser, controller.postLogin);

module.exports = router;