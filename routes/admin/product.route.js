const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const controller = require('../../controllers/admin/product.controller');

router.get('/search', controller.search);

router.get('/list', controller.list);

router.get('/insert', controller.getInsert);

router.post('/insert', urlencodedParser, controller.postInsert);

router.get('/edit/:id', controller.getEdit);

router.post('/edit/:id', urlencodedParser, controller.postEdit);

router.get('/delete/:id', controller.delete);

module.exports = router;