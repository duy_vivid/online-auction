//require('dotenv').config();

const express = require('express');
const app = express();

const productRoute = require('./routes/product.route');
const adminRoute = require('./routes/admin.route');
const indexRoute = require('./routes/index.route')

const port = 3000;

app.set('view engine', 'ejs');
app.set('views','./views');

app.use(express.static('public'));

app.use('/products', productRoute);

app.use('/admin', adminRoute);

app.use('/', indexRoute);

app.listen(port, () => console.log(`Server listening on port ${port}!`));