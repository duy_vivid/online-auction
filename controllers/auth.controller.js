//const md5 = require('md5');

const bcrypt = require('bcryptjs');

const userDb = require('../models/admin/user.model');

module.exports.login = (req, res) => {

    res.render('authenticate/login');
};

module.exports.postLogin = async (req, res) => {
    var email = req.body.email;
    var password = req.body.password;

    const user = await userDb.searchEmail(email);

    /*const N = 1024, r = 8, p = 1;
    const dkLen = 5;

    const pwDb = user.password;
 
    const salt = pwDb.substring(dkLen, pwDb.length);

    console.log(pwDb);
    console.log(pwDb.substring(5, pwDb.length));
    const prehash = password + salt;
    
    const hash = bcrypt.hash(prehash, salt);

    const passwordhash = hash + salt;
    console.log(hash);
    console.log(passwordhash);*/

    if(!user)
    {
        res.render('authenticate/login', {
            errors: [
                'Users does not exists.'
            ],
            values: req.body
        });
        return;
    }

    if(user.password !== password)
    {
        console.log('Wrong password.');
        res.render('authenticate/login', {
            errors: [
                'Wrong password.'
            ],
            values: req.body
        });
        return;
    }

    /*res.cookie('userId', user.id, {
        signed: true
    });*/

    if (user.role.toLowerCase() === 'bidder')
    {
        res.redirect('../');
    }
    if (user.role.toLowerCase() === 'seller')
    {
        res.redirect('../');
    }
    if (user.role.toLowerCase() === 'admin')
    {
        res.redirect('../');
    }
    res.redirect('../');
};