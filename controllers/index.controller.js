const pool = require('../utils/db');

const catModule = require('../models/category');

const catOther = require('../models/other/category.module');

const productOther = require('../models/other/product.module');

const productModule = require('../models/product');

module.exports.sidebar = async (req, res) => {
    const allList = await catModule.all();

    const rootList = await catOther.rootList();

    //res.render('layout/sidebar', { catroots: rootList, cats: allList });

    res.render('guest/index', { catroots: rootList, cats: allList });
};

module.exports.search = async (req, res) => {
    var search = req.query.search;

    const allList = await catModule.all();

    const rootList = await catOther.rootList();

    const pSearch = await productOther.searchByName(search);

    res.render('guest/index', { catroots: rootList, cats: allList, products: pSearch});
};

module.exports.home = async (req, res) => {
    var search = req.query.search;

    const rootList = await catOther.rootList();

    const allList = await catModule.all();

    const pTopGoToEnd = await productOther.topGoToEnd();

    const pTopBestPrice = await productOther.topBestPrice();
    
    const pTopDeal = await productOther.topDeal();

    res.render('index', { catroots: rootList, cats: allList, topGoToEnd: pTopGoToEnd, topBestPrice: pTopBestPrice, topDeal: pTopDeal });
};

module.exports.list = async (req, res) => {
    const allList = await catModule.all();

    const rootList = await catOther.rootList();

    const productList = await productOther.productBid();

    res.render('guest/index', { catroots: rootList, cats: allList, products: productList });
};