const userAdmin = require('../../models/admin/user.model')

module.exports.search = async (req, res) => {
    const search = req.query.search;

    const uSearch = await userAdmin.search(search);

    res.render('admin/users/list', { danhsach: uSearch});
};

module.exports.list = async (req, res) => {
    const uList = await userAdmin.all();

    res.render('admin/users/list', { danhsach: uList});
};

module.exports.getInsert = (req, res) => {
    res.render('admin/users/insert');
};

module.exports.postInsert = async (req, res) => {
    var name = req.body.txtUsername;
    var email = req.body.txtEmail;
    var password = req.body.txtPassword;
    var role = req.body.txtRole;

    const uPInsert = await userAdmin.addUser(name, email, password, role);

    res.redirect('list');
};

module.exports.getEdit = async (req, res) => {
    var idSQL = req.params.id;

    const uGEdit = await userAdmin.getEditUser(idSQL);

    res.render('admin/users/edit', { user: uGEdit });
};

module.exports.postEdit = async (req, res) => {
    var name = req.body.txtUsername;
    var email = req.body.txtEmail;
    var password = req.body.txtPassword;
    var role = req.body.txtRole;
    var idSQL = req.params.id;

    const uPEdit = await userAdmin.postEditUser(idSQL, name, email, password, role);

    res.redirect('../list');
};

module.exports.delete = async (req, res) => {
    var idSQL = req.params.id;

    const uDelete = await userAdmin.deleteUser(idSQL);

    res.redirect('../list');
};

module.exports.upgrade = async (req, res) => {
    var idSQL = req.params.id;

    const uDelete = await userAdmin.upgradeUser(idSQL);

    res.redirect('../list');
};

module.exports.downgrade = async (req, res) => {
    var idSQL = req.params.id;

    const uDelete = await userAdmin.downgradeUser(idSQL);

    res.redirect('../list');
};