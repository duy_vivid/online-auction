const requestAdmin = require('../../models/admin/request.model')

module.exports.search = async (req, res) => {
    const search = req.query.search;

    const rqSearch = await requestAdmin.search(search);

    res.render('admin/requests/list', { danhsach: rqSearch});  
};

module.exports.list = async (req, res) => {
    const rqList = await requestAdmin.all();

    res.render('admin/requests/list', { danhsach: rqList});
};