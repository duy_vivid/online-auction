const categoryAdmin = require('../../models/admin/category.model')

module.exports.search = async (req, res) => {
    const search = req.query.search;

    const cSearch = await categoryAdmin.search(search);

    res.render('admin/categories/list', { danhsach: cSearch});
};

module.exports.list = async (req, res) => {
    const cList = await categoryAdmin.all();

    res.render('admin/categories/list', { danhsach: cList});
};

module.exports.getInsert = async (req, res) => {
    res.render('admin/categories/insert');
};

module.exports.postInsert = async (req, res) => {
    var name = req.body.txtName;
    var root = req.body.numRoot;

    if(root == '')
    {
        root = null;
    }

    const cPInsert = await categoryAdmin.addCategory(name,root);

    res.redirect('list');
};

module.exports.getEdit = async (req, res) => {
    var idSQL = req.params.id;

    const cGEdit = await categoryAdmin.getEditCategory(idSQL);

    res.render('admin/categories/edit', { cat: cGEdit });
};

module.exports.postEdit = async (req, res) => {
    var name = req.body.txtName;
    var root = req.body.numRoot;
    var idSQL = req.params.id;

    if(root == '')
    {
        root = null;
    }

    const cPEdit = await categoryAdmin.postEditCategory(idSQL, name, root);

    res.redirect('../list');
};

module.exports.delete = async (req, res) => {
    var idSQL = req.params.id;

    const cDelete = await categoryAdmin.deleteCategory(idSQL);

    res.redirect('../list');
};