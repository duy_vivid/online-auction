const productAdmin = require('../../models/admin/product.model')

module.exports.search = async (req, res) => {
    const search = req.query.search;

    const pSearch = await productAdmin.search(search);

    res.render('admin/products/list', { danhsach: pSearch});
};

module.exports.list = async (req, res) => {
    const pList = await productAdmin.all();

    res.render('admin/products/list', { danhsach: pList});
};

module.exports.getInsert = async (req, res) => {
    res.render('admin/products/insert');
};

module.exports.postInsert = async (req, res) => {
    var name = req.body.txtProductname;
    var price = req.body.txtPrice;
    var submittedDate = req.body.txtSubmittedDate;
    var closedDate = req.body.txtClosedDate;
    var description = req.body.txtDescription;
    var increment = req.body.txtIncrement;
    var startingPrice = req.body.txtStartingPrice;
    var sellerId = req.body.txtSellerId;
    var categoryId = req.body.txtCategoryId;
    var document_vectors = req.body.txtdocument_vectors;

    const pPInsert = await productAdmin.addProduct(name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors);

    res.redirect('list');
};

module.exports.getEdit = async (req, res) => {
    var idSQL = req.params.id;

    const pGEdit = await productAdmin.getEditProduct(idSQL);

    res.render('admin/products/edit', { product: pGEdit });
};

module.exports.postEdit = async (req, res) => {
    var name = req.body.txtProductname;
    var price = req.body.txtPrice;
    var submittedDate = req.body.txtSubmittedDate;
    var closedDate = req.body.txtClosedDate;
    var description = req.body.txtDescription;
    var increment = req.body.txtIncrement;
    var startingPrice = req.body.txtStartingPrice;
    var sellerId = req.body.txtSellerId;
    var categoryId = req.body.txtCategoryId;
    var document_vectors = req.body.txtdocument_vectors;
    var idSQL = req.params.id;

    const pPEdit = await productAdmin.postEditProduct(idSQL, name, price, submittedDate, closedDate, description, increment, startingPrice, sellerId, categoryId, document_vectors);

    res.redirect('../list');
};

module.exports.delete = async (req, res) => {
    var idSQL = req.params.id;

    const pDelete = await productAdmin.deleteProduct(idSQL);

    res.redirect('../list');
};